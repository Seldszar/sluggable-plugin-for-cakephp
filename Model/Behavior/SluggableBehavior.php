<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link        http://seldszar.fr Seldszar.fr
 * @package     Sluggable.Model.Behavior
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('ModelBehavior', 'Model');
App::uses('Inflector', 'Utility');

/**
 * Slug field management
 *
 * @package     Sluggable.Model.Behavior
 */
class SluggableBehavior extends ModelBehavior {

	public function setup(Model $Model, $settings = array()) {
		$settings = Hash::normalize($settings);
		foreach ($settings as $k => &$v) {
			if (!is_array($v)) {
				$v = array('field' => $v);
			}
			$v = Hash::merge(array(
				'field' => $k . '_slug',
				'replacement' => '_'
			), $v);
		}
		$this->settings = Hash::merge($this->settings, $settings);
	}

	public function beforeSave(Model $Model) {
		foreach ($this->settings as $k => $v) {
			$data = &$Model->data[$Model->alias];
			$field = $v['field'];
			if (isset($data[$k]) && !empty($field)) {
				$value = $data[$k];
				$slug = strtolower(Inflector::slug($value, $v['replacement']));
				$data[$field] = $slug;
			}
		}
		return true;
	}

}
