# Sluggable Plugin for CakePHP #

## Setup ##

### Getting plugin ###

Clone source code from this repository into your `Plugin/Sluggable` directory.

### Load plugin ###

Load the Sluggable plugin in your bootstrap file, remember to use plugin bootstrap, like this :

    Cake::loadPlugin('Sluggable');

## Sluggable Behavior ##

### How to use ###

This view allow to generate pdf files from HTML code, to activate this view, add this line in your model :

    public $actsAs = array(
    	'Sluggable.Sluggable'
	);

### Example use ###

#### Single slug management ####

I want to save into my model slug value of `lorem`, the behavior save it into `lorem_slug` field if exists.

    public $actsAs = array(
        'Sluggable.Sluggable' => array(
            'lorem'
        )
	);

If I want to save the slug into an other field, `ipsum` for example, I simply add the following value into the behavior declaration :

    public $actsAs = array(
        'Sluggable.Sluggable' => array(
            'lorem' => 'ipsum'
        )
	);

#### Multiple slug management ####

    public $actsAs = array(
        'Sluggable.Sluggable' => array(
            'lorem',
            'ipsum'
        )
	);

#### Replacement token customization ####

    public $actsAs = array(
        'Sluggable.Sluggable' => array(
            'lorem' => array(
                'field' => 'ipsum',
                'replacement' => '-'
            )
        )
    );

#### Advanced use ####

    public $actsAs = array(
        'Sluggable.Sluggable' => array(
            'lorem',
            'ipsum' => 'dolor',
            'sit' => array(
                'field' => 'amet',
                'replacement' => '-'
            )
        )
    );

## Branch strategy ##

This repository is splitted into following branches :

 * **master** : releases and stable revisions
 * **develop** : new features
